#ifndef SDL_HANDLER_H
# define SDL_HANDLER_H

#include "SDL.h"
#include <vector>
#include <string>

class SDLHandler {

public:
	SDLHandler( int width, int height, std::string name = "SDL Project" );
	~SDLHandler( void );
	int		handleInput();
	void	display();
    void	putpixel(unsigned int x, unsigned int y, unsigned int colour);
	void	drawLine(int x1, int y1, int x2, int y2, int colour);
	void	clearpixels();
	void	fill(int colour);
	void 	rect(unsigned int x1, unsigned int y1, unsigned int x2, unsigned int y2, unsigned int colour);
	void	setWinName(std::string name);
	std::vector<int>	&getMove() { return pressed; };

private:
	int		width;
	int		height;
	
	SDL_Window			*window;
	SDL_Surface			*surface;
	SDL_Renderer		*renderer;
	SDL_Event			event;
	std::vector<int>	pressed;


};

#endif
