from PIL import Image

from colour import getcol, mkcol, mkcolt

class PImage():
    average = (0, 0, 0)
    image = None
    
    def __init__(self, img=None, *args, **kwargs):
        self.image_file = img
        pass

    def process_image(self):
        try:
            self.image = Image.open(self.image_file)
        except:
            print("Unable to open file: " + self.image_file)
            print("Make sure to include picture extension, eg 'example.png', 'example.jpg'")
            return
        
        im = self.image.convert('RGB')

        pix = im.load()
        pc = 0
        for x in range(im.width):
            for y in range(im.height):
                pc += mkcolt(pix[x, y])

        avg = pc / (im.width * im.height * mkcol(255, 255, 255))
        thresh = round( mkcol(255, 255, 255) * avg / 1.15) 
        self.average = getcol(avg)


















