
def getcol(code):
	blue =  code & 255
	green = (code >> 8) & 255
	red =   (code >> 16) & 255
	return (red, green, blue)

def mkcol(red, green, blue):
	return red * 256 * 256 + green * 256 + blue

def mkcolt(tuple):
	return tuple[0] * 256 * 256 + tuple[1] * 256 + tuple[2]
