import draw
import env
from math import pi, sin, cos

WHITE = (255, 255, 255)

SCREEN_WIDTH = 800
SCREEN_HEIGHT = 600

MAP = 'wolf.map'

def angle(angle):
	return (angle * pi) / 180

def draw_walls():
	for wall in env.WALLS:
		draw.draw_line(wall.x1, wall.y1, wall.x1, wall.y2, (255, 0, 0))
		draw.draw_line(wall.x2, wall.y1, wall.x2, wall.y2, (255, 0, 0))
		draw.draw_line(wall.x1, wall.y1, wall.x2, wall.y1, (255, 0, 0))
		draw.draw_line(wall.x1, wall.y2, wall.x2, wall.y2, (255, 0, 0))

def mini_map(dis, a):
	x1 = env.X
	y1 = env.Y
	x2 = x1 + sin(a) * dis;
	y2 = y1 + cos(a) * dis;
	draw.draw_line(x1, y1, x2, y2, (255, 0, 0))
	# x2 = x1 + sin(env.POV - env.FOV / 2) * dis;
	# y2 = y1 + cos(env.POV - env.FOV / 2) * dis;
	# draw.draw_line(x1, y1, x2, y2, WHITE)


def read_map():
	mapdata = open(MAP, 'r+')
	line = '0'
	y = 0
	while len(line) > 0:
		line = mapdata.readline()
		y += 1
		x = 0
		for char in line:
			x += 1
			if char == '1':
				x1 = x * env.SCALE
				y1 = y * env.SCALE
				env.WALLS.append(
					env.Wall(
						x1,
						y1,
						x1 + env.SCALE,
						y1 + env.SCALE
					)
				)
			elif char == 'X':
				env.X = x * env.SCALE
				env.Y = y * env.SCALE


def draw_line(x1, y1, x2, y2, colour):
	dx = x2 - x1
	dy = y2 - y1
	steps = abs(dx) if abs(dx) > abs(dy) else abs(dy)
	xinc = dx / steps
	yinc = dy / steps
	x = x1
	y = y1
	i = 0
	while i <= steps:
		draw.putpixel(x, y, colour)
		x += xinc
		y += yinc
		i += 1


def draw_wall_line(size, x, colour):
	if env.ANGLE > env.POV:
		real_angle = env.ANGLE - env.POV
	else:
		real_angle = env.POV - env.ANGLE
	size = size * cos(angle(real_angle))

	line_height = (SCREEN_HEIGHT / size) * env.SCALE
	draw_start = -line_height / 2 + SCREEN_HEIGHT / 2
	draw_end = line_height / 2 + SCREEN_HEIGHT / 2

	draw.draw_line(x, draw_start, x, draw_end, colour)


def get_wall_color():
	return WHITE


def intersect(wall):
	angle = (env.ANGLE * pi) / 180
	s = sin(angle)
	c = cos(angle)
	if s == 0 or c == 0:
		return 0
	txmin = (wall.x1 - env.X) / s
	txmax = (wall.x2 - env.X) / s
	tymin = (wall.y1 - env.Y) / c
	tymax = (wall.y2 - env.Y) / c
	if txmin > txmax:
		temp = txmin
		txmin = txmax
		txmax = temp
	if tymin > tymax:
		temp = tymin
		tymin = tymax
		tymax = temp
	if txmin > tymax or tymin > txmax:
		return 0
	tmin = txmin
	tmax = txmax
	if tymin > txmin:
		tmin = tymin
	if tymax > txmax:
		tmax = tymax
		
	return tmax if tmax < tmin else tmin


def collision():
	closest_distance = 0
	for wall in env.WALLS:
		distance = intersect(wall)
		if distance > 0:
			if distance < closest_distance or closest_distance == 0:
				closest_distance = distance
	colour = get_wall_color()
	return closest_distance


def ray_caster():
	env.ANGLE = env.POV - (env.FOV / 2);
	ray_step = env.FOV / SCREEN_WIDTH;
	step = 0
	while step < SCREEN_WIDTH:
		distance = collision()
		if distance > 0:
			draw_wall_line(distance, step, WHITE)
		mini_map(dis=distance, a=((env.ANGLE * pi) / 180))
		env.ANGLE += ray_step
		step += 45
	draw_walls()
	env.POV += 1
	


def main():
	read_map()
	draw.init("Wolf3D", size=(SCREEN_WIDTH, SCREEN_HEIGHT))
	while True:
		print('loop')
		ray_caster()
		draw.update()


if __name__ == "__main__":
	main()
