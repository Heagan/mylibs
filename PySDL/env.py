import draw


SCALE = 100
ANGLE = 0

WALLS = []

POV = 0
FOV = 30
X = 0
Y = 0


class Wall:
	def __init__(self, x1, y1, x2, y2):
		self.x1 = x1
		self.x2 = x2
		self.y1 = y1
		self.y2 = y2
