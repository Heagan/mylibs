from os import system

a = [
    "connector-connection-api",
    "connector-connection-tasks",
    "connector-twitterads-api",
    "connector-twitterads-tasks",
    "connector-email-api",
    "connector-email-tasks",
    "connector-pushsftp-api",
    "connector-pushsftp-tasks",
    "connector-amnetdcpm-api",
    "connector-amnetdcpm-tasks",
    "connector-pullsftp-api",
    "connector-pullsftp-tasks",
    "connector-pullftp-api",
    "connector-pullftp-tasks",
    "connector-ads360-api",
    "connector-ads360-tasks",
    "connector-googleads-api",
    "connector-googleads-tasks",
    "connector-dcmapireport-api",
    "connector-dcmapireport-tasks",
    "connector-dcmdtv2-api",
    "connector-dcmdtv2-tasks",
    "connector-facebookads-api",
    "connector-facebookads-tasks",
    "connector-contract-api",
    "connector-job-api",
    "connector-swagger-api"
]

for aa in a:
    system(f'echo "Running: {aa}"')
    system(f'sh my_run_test_script.sh {aa}')
    system(f'echo "Finished: {aa}"')

