# This code sample uses the 'requests' library:
# http://docs.python-requests.org
import requests
from requests.auth import HTTPBasicAuth
import json


url = "https://dotmodus.atlassian.net/rest/api/3/issue/CONMAN-439/worklog"
auth = HTTPBasicAuth("georges@dotmodus.com", "XhadLJuN9ogqrpz5O4v0C9E8")

url = "https://jira-engineering.dentsuaegis.com/rest/api/3/issue/DR-20/worklog"
auth = HTTPBasicAuth("george.sferopoulos@dentsuaegis.com", "hLces1BJMTsXVrfmxnHH265B")


headers = {
   "Accept": "application/json",
   "Content-Type": "application/json"
}

payload = json.dumps({
  "timeSpent": "1d",
  "started": "2019-11-19T12:25:52.254+0000"
})

response = requests.request(
   "POST",
   url,
   data=payload,
   headers=headers,
   auth=auth
)

print(json.dumps(json.loads(response.text), sort_keys=True, indent=4, separators=(",", ": ")))
